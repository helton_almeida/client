## CLIENT

## Descrição do projeto

Esse projeto consiste na manutenção de dados dos clientes. Podendo realizar ações de consulta, inserção, atualização e deleção de dados.

## Pré requisitos

- [Java 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Maven 3](https://maven.apache.org/download.cgi)
- [PostgreSQL](https://www.postgresql.org/)

## Dependências utilizadas

- Spring Boot Framework: Controlar o projeto e diminuir a necessidade de configurações.
- Apache commons: Facilitar a escrita do código com classes utilitarias
- Postgresql: Integração com o SGBD
- Swagger: Documentação da API
- Flyway: Automatizar  a criação de scripts SQL 
- Junit: Criação de testes unitários
- Mockito: Criação de testes unitários

## Para funcionar 

- Download do projeto

No terminal, clone o projeto:

```
git clone https://helton_almeida@bitbucket.org/helton_almeida/client.git
```

- Configurando SGBD

Visto a aplicação utilizar o PostresSQL, podemos obter seu instalador atraves do download [PostgreSQL](https://www.postgresql.org/) 
ou tendo o [Docker](https://www.docker.com/) configurado, para obter uma instância execute o comando abaixo.  

```
docker run -d --name postgres -p 5432:5432 hpalmeida/helton-postgres
```

Para facilitar o acesso ou caso queira fazer alguma mudança na imagem, o arquivo Dockerfile que originou a imagem hpalmeida/helton-postgres encontra-se no caminho infra\docker a partir da raiz do projeto.

Quanto a configuração das credencias para acesso ao banco, no arquivo application.yml já contém as configurações necessárias para a aplicação se conectar ao PostgreSQL.

Na primeira vez que estiver executando o projeto, através do Flaway sera executado no banco os scripts contidos no caminho src\main\resources\db para criação de tabela e massa de dados.

## Para apreciação

- Swagger 

```
http://localhost:5000/swagger-ui.html
```
- Postman

Na raiz do projeto esta contido o arquivo CLIENT_API.postman_collection.json





