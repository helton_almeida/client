package com.builders.client.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CpfValidatorTest {
	
	@Test
	public void isValidCpf_mustReturnTrueWhenCpfIsValid() {
		String cpf = "80299466000";
		
		Boolean result = CpfValidator.isValid(cpf);
		
		Assert.assertTrue(result);
	}
	
	@Test
	public void isValidCpf_mustReturnFalseWhenCpfIsNULL() {
		String cpf = null;
		
		Boolean result = CpfValidator.isValid(cpf);
		
		Assert.assertFalse(result);
	}
	
	@Test
	public void isValidCpf_mustReturnFalseWhenCpfIsIncomplete() {
		String cpf = "8029946600";
		
		Boolean result = CpfValidator.isValid(cpf);
		
		Assert.assertFalse(result);
	}
	
	@Test
	public void isValidCpf_mustReturnFalseWhenCpfIsNotValid() {
		String cpf = "12345612314";
		
		Boolean result = CpfValidator.isValid(cpf);
		
		Assert.assertFalse(result);
	}
	
	@Test
	public void isValidCpf_shouldReturnFalseWhenCpfIsARepeatedSequence() {
		String cpf = "11111111111";
		
		Boolean result = CpfValidator.isValid(cpf);
		
		Assert.assertFalse(result);
	}

}
