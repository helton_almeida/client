package com.builders.client.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import com.builders.client.exception.GenericException;

@RunWith(MockitoJUnitRunner.class)
public class ValidatorTest {
	
    @Test
    public void checkArgument_mustNotContainErrorinList(){
        Validator validator = Validator.newInstance();

        validator.checkArgument(true, "Erro");

        Assert.assertTrue(validator.getErrors().isEmpty());
    }

    @Test
    public void checkArgument_mustContainErrorinList(){
        Validator validator = Validator.newInstance();

        validator.checkArgument(false, "Erro");

        Assert.assertEquals(1, validator.getErrors().size());
    }

    @Test(expected = GenericException.class)
    public void checkArgument_mustThrowException(){
        Validator.checkArgument(false, new GenericException());
    }

}
