package com.builders.client.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import com.builders.client.dto.ClientBirthDateRequestDTO;
import com.builders.client.dto.ClientRequestDTO;
import com.builders.client.dto.ClientResponseDTO;
import com.builders.client.exception.ClientNotFounException;
import com.builders.client.exception.InvalidBirthDateException;
import com.builders.client.exception.ValidationException;
import com.builders.client.model.Client;
import com.builders.client.repository.ClientRepository;

@RunWith(MockitoJUnitRunner.class)
public class ClienteServiceTest {
	
	@InjectMocks
	private ClientService clientService;
	
	@Mock
	private ClientRepository clientRepository;
	
	@Test
	public void findBy_mustReturnClientList() {
	    Integer page = 0;
	    Integer size = 10;
	    String name = "João";
	    String cpf = "80299466000";
	    Mockito.when(clientRepository.findBy(Mockito.anyString(), Mockito.anyString(), Mockito.any())).thenReturn(new PageImpl<>(new ArrayList<>()));
	    
	    Page<ClientResponseDTO> result = clientService.findBy(cpf, name, page, size);
	    
	    Assert.assertNotNull(result);
	}
	
	@Test(expected = ValidationException.class)
	public void findBy_mustThrowExceptionWhenPageIsNULL() {
		Integer page = null;
		Integer size = 10;
	    
	    clientService.findBy(null, null, page, size);
	}
	
	@Test(expected = ValidationException.class)
	public void findBy_mustThrowExceptionWhenPageIsLessThanZero() {
		Integer page = -1;
		Integer size = 10;
	    
	    clientService.findBy(null, null, page, size);
	}
	
	@Test(expected = ValidationException.class)
	public void findBy_mustThrowExceptionWhenSizeIsNULL() {
		Integer page = 0;
		Integer size = null;
	    
	    clientService.findBy(null, null, page, size);
	}
	
	@Test(expected = ValidationException.class)
	public void findBy_mustThrowExceptionWhenSizeIsLessThanZero() {
		Integer page = 0;
		Integer size = -1;
	    
	    clientService.findBy(null, null, page, size);
	}
	
	@Test(expected = ValidationException.class)
	public void findBy_mustThrowExceptionWhenSizeEqualsZero() {
		Integer page = 0;
		Integer size = 0;
	    
	    clientService.findBy(null, null, page, size);
	}
	
	@Test
	public void save_mustSaveClient() {
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("João", "80299466000", LocalDate.of(1994, 05, 06));
		Client client = new Client(1l, "João", "80299466000", LocalDate.of(1994, 05, 06));
		Mockito.when(clientRepository.save(Mockito.any())).thenReturn(client);
	    
	    ClientResponseDTO result = clientService.save(requestClientDTO);
	    
	    Assert.assertNotNull(result.getId());
	    Assert.assertNotNull(result.getName());
	    Assert.assertNotNull(result.getCpf());
	    Assert.assertNotNull(result.getBirthDate());
	}
	
	@Test(expected = ValidationException.class)
	public void save_mustThrowExceptionWhenNameIsNULL() {
		ClientRequestDTO requestClientDTO = new ClientRequestDTO(null, "80299466000", LocalDate.of(1994, 05, 06));
	    
	    clientService.save(requestClientDTO);
	}
	
	@Test(expected = ValidationException.class)
	public void save_mustThrowExceptionWhenCpfIsNULL() {
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("João", null, LocalDate.of(1994, 05, 06));
	    
	    clientService.save(requestClientDTO);
	}
	
	@Test(expected = ValidationException.class)
	public void save_mustThrowExceptionWhenBirthDateIsNULL() {
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("João", "80299466000", null);
	    
	    clientService.save(requestClientDTO);
	}
	
	@Test(expected = ValidationException.class)
	public void save_mustThrowExceptionWhenBirthDateIsGreaterThanCurrentDate() {
		Integer year = LocalDate.now().plusYears(1).getYear();
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("", "80299466000", LocalDate.of(year, 05, 06));
	    
		clientService.save(requestClientDTO);
	}
	
	@Test
	public void update_mustUpdateClient() {
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("João Nogeira", "06682034040", LocalDate.of(1994, 05, 07));
		Client client = new Client(1l, "João", "80299466000", LocalDate.of(1994, 05, 06));
		Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(client));
		Mockito.when(clientRepository.save(Mockito.any())).thenReturn(client);
	    
	    ClientResponseDTO result = clientService.update(1l, requestClientDTO);
	    
	    Assert.assertEquals(requestClientDTO.getName(), result.getName());
	    Assert.assertEquals(requestClientDTO.getCpf(), result.getCpf());
	    Assert.assertEquals(requestClientDTO.getBirthDate(), result.getBirthDate());
	}
	
	@Test(expected = ClientNotFounException.class)
	public void update_update_mustThrowExceptionWhenClientIsNotFound() {
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("João", "80299466000", LocalDate.of(1994, 05, 06));
		Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
	    
		clientService.update(1l, requestClientDTO);
	}
	
	@Test(expected = ValidationException.class)
	public void update_mustThrowExceptionWhenNameIsEmpty() {
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("", "80299466000", LocalDate.of(1994, 05, 06));
	    
	    clientService.update(1l, requestClientDTO);
	}
	
	@Test(expected = ValidationException.class)
	public void update_mustThrowExceptionWhenCpfIsEmpty() {
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("João", "", LocalDate.of(1994, 05, 06));
	    
	    clientService.update(1l, requestClientDTO);
	}
	
	@Test(expected = ValidationException.class)
	public void update_mustThrowExceptionWhenBirthDateIsNULL() {
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("João", "80299466000", null);
	    
		 clientService.update(1l, requestClientDTO);
	}
	
	@Test(expected = ValidationException.class)
	public void update_mustThrowExceptionWhenBirthDateIsGreaterThanCurrentDate() {
		Integer year = LocalDate.now().plusYears(1).getYear();
		ClientRequestDTO requestClientDTO = new ClientRequestDTO("", "80299466000", LocalDate.of(year, 05, 06));
	    
		clientService.update(1l, requestClientDTO);
	}
	
	@Test
	public void delete_mustDeleteClient() {
		Client client = new Client(1l, "João", "80299466000", LocalDate.of(1994, 05, 06));
		Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(client));
		
		clientService.delete(1l);
		
		Mockito.verify(clientRepository).delete(client);
	}
	
	@Test(expected = ClientNotFounException.class)
	public void delete_mustThrowExceptionWhenClientIsNotFound() {
		Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
	    
		clientService.delete(1l);
	}
	
	@Test
	public void updateBirthDate_mustUpdateBirthDate() {
		ClientBirthDateRequestDTO clientBirthDateRequestDTO = new ClientBirthDateRequestDTO(LocalDate.of(1994, 05, 07));
		Client client = new Client(1l, "João", "80299466000", LocalDate.of(1994, 05, 06));
		Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(client));
		Mockito.when(clientRepository.save(Mockito.any())).thenReturn(client);
	    
	    ClientResponseDTO result = clientService.updateBirthDate(1l, clientBirthDateRequestDTO);
	    
	    Assert.assertEquals(clientBirthDateRequestDTO.getBirthDate(), result.getBirthDate());
	}
	
	@Test(expected = ClientNotFounException.class)
	public void updateBirthDate_mustThrowExceptionWhenClientIsNotFound() {
		ClientBirthDateRequestDTO clientBirthDateRequestDTO = new ClientBirthDateRequestDTO(LocalDate.of(1994, 05, 07));
		Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));
	    
		clientService.updateBirthDate(1l, clientBirthDateRequestDTO);
	}
	
	@Test(expected = InvalidBirthDateException.class)
	public void updateBirthDate_mustThrowExceptionWhenBirthDateIsNULL() {
		ClientBirthDateRequestDTO clientBirthDateRequestDTO = new ClientBirthDateRequestDTO(null);
		Client client = new Client(1l, "João", "80299466000", LocalDate.of(1994, 05, 06));
		Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(client));
	    
		clientService.updateBirthDate(1l, clientBirthDateRequestDTO);
	}
	
	@Test(expected = InvalidBirthDateException.class)
	public void updateBirthDate_mustThrowExceptionWhenBirthDateIsGreaterThanCurrentDate() {
		Integer year = LocalDate.now().plusYears(1).getYear();
		ClientBirthDateRequestDTO clientBirthDateRequestDTO = new ClientBirthDateRequestDTO(LocalDate.of(year, 05, 06));
		Client client = new Client(1l, "João", "80299466000", LocalDate.of(1994, 05, 06));
		Mockito.when(clientRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(client));
	    
		clientService.updateBirthDate(1l, clientBirthDateRequestDTO);
	}

}
