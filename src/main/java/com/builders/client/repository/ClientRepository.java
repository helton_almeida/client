package com.builders.client.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.builders.client.dto.ClientResponseDTO;
import com.builders.client.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {

	@Query(value = "SELECT new com.builders.client.dto.ClientResponseDTO(client.id, client.name, client.cpf, client.birthDate) " +
					"FROM Client client " +
					"WHERE (:cpf IS NULL OR client.cpf = :cpf) " +
					"AND (:name IS NULL OR LOWER(client.name) LIKE %:name%)")
	Page<ClientResponseDTO> findBy(@Param("cpf") String cpf, @Param("name") String name, Pageable pageable);

	@Query(value = "SELECT client FROM Client client WHERE client.id = :id ")
	Optional<Client> findById(@Param("id") Long id);

}
