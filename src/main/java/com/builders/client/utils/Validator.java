package com.builders.client.utils;

import java.util.ArrayList;
import java.util.List;

import com.builders.client.exception.GenericException;
import com.builders.client.exception.ValidationException;

public final class Validator {

	private List<String> errors = new ArrayList<>();

	private Validator() {

	}

	public static Validator newInstance() {
		return new Validator();
	}

	public Validator checkArgument(boolean condition, String message) {
		if (!condition) {
			errors.add(message);
		}
		return this;
	}

	public void throwExceptions() {
		if (!errors.isEmpty()) {
			throw new ValidationException(errors);
		}
	}

	public static void checkArgument(boolean condition, GenericException exception) {
		if (!condition) {
			throw exception;
		}
	}
	
	public List<String> getErrors() {
		return errors;
	}

}
