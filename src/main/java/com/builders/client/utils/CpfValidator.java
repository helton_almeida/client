package com.builders.client.utils;

public final class CpfValidator {

	private static final int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

	private CpfValidator() {

	}

	public static boolean isValid(String cpf) {
		if ((cpf == null) || (cpf.length() != 11)) {
			return false;
		}
		cpf = cpf.trim();
		int digito1;
		int digito2;
		try {
			boolean allEqual = true;
			for (int i = 0; i < cpf.length() - 1; i++) {
				if (cpf.charAt(i) != cpf.charAt(i + 1)) {
					allEqual = false;
					break;
				}
			}
			if (allEqual)
				return false;
			digito1 = calculateDigit(cpf.substring(0, 9), pesoCPF);
			digito2 = calculateDigit(cpf.substring(0, 9) + digito1, pesoCPF);
		} catch (NumberFormatException ex) {
			return false;
		}
		return cpf.equals(cpf.substring(0, 9) + digito1 + digito2);
	}

	private static int calculateDigit(String str, int[] peso) {
		int sum = 0;
		int digito;
		for (int index = str.length() - 1; index >= 0; index--) {
			digito = Integer.parseInt(str.substring(index, index + 1));
			sum += digito * peso[peso.length - str.length() + index];
		}
		sum = 11 - sum % 11;
		return sum > 9 ? 0 : sum;
	}

}
