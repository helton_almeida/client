package com.builders.client.handler;

import java.util.Arrays;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.builders.client.dto.ExceptionResponseDTO;
import com.builders.client.exception.GenericException;
import com.builders.client.exception.ValidationException;

@ControllerAdvice
public class GenericExceptionHandler extends ResponseEntityExceptionHandler {

	private Logger log = LoggerFactory.getLogger(GenericExceptionHandler.class);
	
	@ExceptionHandler(ValidationException.class)
	protected ResponseEntity<ExceptionResponseDTO> handleException(ValidationException ex) {
		ExceptionResponseDTO responseMessage = ExceptionResponseDTO.Builder.newBuilder()
				.status(ValidationException.class.getAnnotation(ResponseStatus.class).code().value())
				.messages(ex.getErrors())
				.build();
		
		return new ResponseEntity<>(responseMessage,
				ValidationException.class.getAnnotation(ResponseStatus.class).code());
	}

	@ExceptionHandler(GenericException.class)
	protected HttpEntity<ExceptionResponseDTO> handleException(GenericException ex) {
		log.error("GenericExceptionHandler", ex);
		ResponseStatus response = ex.getClass().getAnnotation(ResponseStatus.class);
		HttpStatus status = Objects.nonNull(response) ? response.code() : HttpStatus.INTERNAL_SERVER_ERROR;
		String message = getMessage(ex, response);
		ExceptionResponseDTO responseMessage = ExceptionResponseDTO.Builder.newBuilder()
																	.status(status.value())
																	.messages(Arrays.asList(message))
																	.build();
		return new ResponseEntity<>(responseMessage, status);
	}

	private String getMessage(GenericException ex, ResponseStatus response) {
		return Objects.nonNull(response) && StringUtils.isNotBlank(response.reason()) 
				? response.reason()	: ex.getMessage();
	}

}
