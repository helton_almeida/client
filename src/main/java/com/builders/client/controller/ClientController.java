package com.builders.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.builders.client.dto.ClientBirthDateRequestDTO;
import com.builders.client.dto.ClientRequestDTO;
import com.builders.client.dto.ClientResponseDTO;
import com.builders.client.dto.MessageResponseDTO;
import com.builders.client.service.ClientService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(path = "/clients")
public class ClientController {

	@Autowired
	private ClientService clientService;

	@ApiOperation(value = "Recupera as informações dos cliente(s).",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "GET",
            response = Page.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Parâmetro page inválido | Parâmetro size inválido")
    })
	@GetMapping
	public Page<ClientResponseDTO> findBy(@RequestParam(required = false) String cpf,
			@RequestParam(required = false) String name, @RequestParam Integer page, @RequestParam Integer size) {
		return clientService.findBy(cpf, name, page, size);
	}

	@ApiOperation(value = "Realiza a inserção de um cliente.",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "POST",
            response = ClientResponseDTO.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Nome é obrigatório | Data de nascimento inválida | Cpf inválido")
    })
	@PostMapping
	public ClientResponseDTO save(@RequestBody ClientRequestDTO clientRequestDTO) {
		return clientService.save(clientRequestDTO);
	}

	@ApiOperation(value = "Realiza a alteração de um cliente.",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "PUT",
            response = ClientResponseDTO.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"), 
            @ApiResponse(code = 400, message = "Nome é obrigatório | Data de nascimento inválida | Cpf inválido"),
            @ApiResponse(code = 404, message = "Cliente não encontrado")
    })
	@PutMapping("/{id}")
	public ClientResponseDTO update(@PathVariable Long id, @RequestBody ClientRequestDTO clientRequestDTO) {
		return clientService.update(id, clientRequestDTO);
	}

	@ApiOperation(value = "Realiza a exclusão de um cliente.",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "DELETE",
            response = ClientResponseDTO.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"), 
            @ApiResponse(code = 404, message = "Cliente não encontrado")
    })
	@DeleteMapping("/{id}")
	public MessageResponseDTO delete(@PathVariable Long id) {
		clientService.delete(id);
		return MessageResponseDTO.Builder.newBuilder().addMessage("Cliente deletado com sucesso").build();
	}
	
	@ApiOperation(value = "Realiza a alteração da data de nascimento do cliente",
            consumes = "application/json",
            produces = "application/json",
            httpMethod = "PATCH",
            response = ClientResponseDTO.class)
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"), 
            @ApiResponse(code = 400, message = "Data de nascimento inválida"),
            @ApiResponse(code = 404, message = "Cliente não encontrado")

    })
	@PatchMapping("/{id}")
	public ClientResponseDTO updateBirthDate(@PathVariable Long id, @RequestBody ClientBirthDateRequestDTO clientBirthDateRequestDTO) {
		return clientService.updateBirthDate(id, clientBirthDateRequestDTO);
	}

}
