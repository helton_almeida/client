package com.builders.client.service;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.builders.client.convert.ClientConvert;
import com.builders.client.dto.ClientBirthDateRequestDTO;
import com.builders.client.dto.ClientRequestDTO;
import com.builders.client.dto.ClientResponseDTO;
import com.builders.client.exception.ClientNotFounException;
import com.builders.client.exception.InvalidBirthDateException;
import com.builders.client.model.Client;
import com.builders.client.repository.ClientRepository;
import com.builders.client.utils.CpfValidator;
import com.builders.client.utils.Validator;

@Service
public class ClientService {

	@Autowired
	private ClientRepository clientRepository;

	public Page<ClientResponseDTO> findBy(String cpf, String name, Integer page, Integer size) {
		validate(page, size);
		PageRequest pageRequest = PageRequest.of(page, size, Sort.Direction.ASC, "name");
		return clientRepository.findBy(cpf, treatFilter(name), pageRequest);
	}

	public ClientResponseDTO save(ClientRequestDTO clientRequestDTO) {
		validate(clientRequestDTO);
		Client client = ClientConvert.convert(clientRequestDTO);
		return ClientConvert.convert(clientRepository.save(client));
	}

	public ClientResponseDTO update(Long id, ClientRequestDTO clientRequestDTO) {
		validate(clientRequestDTO);
		Optional<Client> clientOptional = clientRepository.findById(id);
		Validator.checkArgument(clientOptional.isPresent(), new ClientNotFounException());
		Client client = clientOptional.get();
		Client updatedClient = ClientConvert.convert(clientRequestDTO);
		client.update(updatedClient);
		return ClientConvert.convert(clientRepository.save(client));
	}

	public void delete(Long id) {
		Optional<Client> clientOptional = clientRepository.findById(id);
		Validator.checkArgument(clientOptional.isPresent(), new ClientNotFounException());
		clientRepository.delete(clientOptional.get());
	}

	public ClientResponseDTO updateBirthDate(Long id, ClientBirthDateRequestDTO clientBirthDateRequestDTO) {
		Optional<Client> clientOptional = clientRepository.findById(id);
		Validator.checkArgument(clientOptional.isPresent(), new ClientNotFounException());
		Validator.checkArgument(isValidBirthDate(clientBirthDateRequestDTO.getBirthDate()), new InvalidBirthDateException());
		Client client = clientOptional.get();
		client.setBirthDate(clientBirthDateRequestDTO.getBirthDate());
		return ClientConvert.convert(clientRepository.save(client));
	}
	
	private String treatFilter(String name) {
		return StringUtils.isNotBlank(name) ? name.toLowerCase() : null;
	}
	
	private void validate(Integer page, Integer size) {
		Validator validator = Validator.newInstance();
		validator.checkArgument(Objects.nonNull(page) && page >= 0, "Parâmetro page inválido");
		validator.checkArgument(Objects.nonNull(size) && size > 0, "Parâmetro size inválido");
		validator.throwExceptions();
	}
	
	private void validate(ClientRequestDTO clientRequestDTO) {
		Validator validator = Validator.newInstance();
		validator.checkArgument(StringUtils.isNotBlank(clientRequestDTO.getName()), "Nome é obrigatório");
		validator.checkArgument(isValidBirthDate(clientRequestDTO.getBirthDate()), "Data de nascimento inválida");
		validator.checkArgument(StringUtils.isNotBlank(clientRequestDTO.getCpf()) 
				&& CpfValidator.isValid(clientRequestDTO.getCpf()), "Cpf inválido");
		validator.throwExceptions();
	}
	
	private Boolean isValidBirthDate(LocalDate date) {
		return (Objects.nonNull(date)) && (date.compareTo(LocalDate.now()) <= 0);
	}

}
