package com.builders.client.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Data de nascimento inválida")
public class InvalidBirthDateException extends GenericException {

	private static final long serialVersionUID = 1L;

}
