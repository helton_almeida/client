package com.builders.client.dto;

import java.util.ArrayList;
import java.util.List;

public class MessageResponseDTO {

	private List<String> messages;

	public MessageResponseDTO(List<String> messages) {
		this.messages = messages;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public static final class Builder {
		private List<String> messages;

		private Builder() {
			messages = new ArrayList<>();
		}

		public static MessageResponseDTO.Builder newBuilder() {
			return new MessageResponseDTO.Builder();
		}

		public MessageResponseDTO.Builder messages(List<String> messages) {
			this.messages = messages;
			return this;
		}

		public MessageResponseDTO.Builder addMessage(String message) {
			this.messages.add(message);
			return this;
		}

		public MessageResponseDTO build() {
			return new MessageResponseDTO(messages);
		}
	}

}
