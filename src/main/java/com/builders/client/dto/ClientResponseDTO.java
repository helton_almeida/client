package com.builders.client.dto;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class ClientResponseDTO {

	private Long id;

	private String name;

	private String cpf;

	private Long age;

	private LocalDate birthDate;
	
	public ClientResponseDTO() {
		
	}

	public ClientResponseDTO(Long id, String name, String cpf, LocalDate birthDate) {
		this.id = id;
		this.name = name;
		this.cpf = cpf;
		this.birthDate = birthDate;
		this.age = ageCalculate(birthDate);
	}

	private Long ageCalculate(LocalDate birthDate) {
		return ChronoUnit.YEARS.between(birthDate, LocalDate.now());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Long getAge() {
		return age;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public static final class Builder {

		private Long id;

		private String name;

		private String cpf;

		private LocalDate birthDate;

		public static Builder newBuilder() {
			return new Builder();
		}

		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder cpf(String cpf) {
			this.cpf = cpf;
			return this;
		}

		public Builder birthDate(LocalDate birthDate) {
			this.birthDate = birthDate;
			return this;
		}

		public ClientResponseDTO build() {
			return new ClientResponseDTO(id, name, cpf, birthDate);
		}
	}

}
