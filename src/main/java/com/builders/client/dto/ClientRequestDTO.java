package com.builders.client.dto;

import java.time.LocalDate;

import org.apache.commons.lang3.StringUtils;

public class ClientRequestDTO {

	private String name;

	private String cpf;

	private LocalDate birthDate;
	
	public ClientRequestDTO() {
		
	}

	public ClientRequestDTO(String name, String cpf, LocalDate birthDate) {
		this.name = name;
		this.cpf = cpf;
		this.birthDate = birthDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = StringUtils.isNotBlank(cpf) ? cpf.replaceAll("[^\\d]+", "") : cpf;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public static final class Builder {

		private String name;

		private String cpf;

		private LocalDate birthDate;

		public static Builder newBuilder() {
			return new Builder();
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder cpf(String cpf) {
			this.cpf = cpf;
			return this;
		}

		public Builder birthDate(LocalDate birthDate) {
			this.birthDate = birthDate;
			return this;
		}

		public ClientRequestDTO build() {
			return new ClientRequestDTO(name, cpf, birthDate);
		}
	}

}
