package com.builders.client.dto;

import java.util.ArrayList;
import java.util.List;

public class ExceptionResponseDTO {

	private int status;
	private List<String> messages;

	public ExceptionResponseDTO(int status, List<String> messages) {
		this.status = status;
		this.messages = messages;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}

	public static final class Builder {
		private int status;
		private List<String> messages;

		private Builder() {
			messages = new ArrayList<>();
		}

		public static Builder newBuilder() {
			return new Builder();
		}

		public Builder status(int status) {
			this.status = status;
			return this;
		}

		public Builder messages(List<String> messages) {
			this.messages = messages;
			return this;
		}

		public Builder addMessage(String message) {
			this.messages.add(message);
			return this;
		}

		public ExceptionResponseDTO build() {
			return new ExceptionResponseDTO(status, messages);
		}
	}
}
