package com.builders.client.dto;

import java.time.LocalDate;

public class ClientBirthDateRequestDTO {

	private LocalDate birthDate;

	public ClientBirthDateRequestDTO() {
		
	}

	public ClientBirthDateRequestDTO(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

}
