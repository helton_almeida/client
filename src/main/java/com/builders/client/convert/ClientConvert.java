package com.builders.client.convert;

import com.builders.client.dto.ClientRequestDTO;
import com.builders.client.dto.ClientResponseDTO;
import com.builders.client.model.Client;

public final class ClientConvert {
	
	private ClientConvert() {
		
	}
	
	public static Client convert(ClientRequestDTO clientRequestDTO) {
		return Client.Builder.newBuilder()
							.name(clientRequestDTO.getName())
							.cpf(clientRequestDTO.getCpf())
							.birthDate(clientRequestDTO.getBirthDate())
							.build();
	}
	
	public static ClientResponseDTO convert(Client client) {
		return ClientResponseDTO.Builder.newBuilder()
										.id(client.getId())
										.name(client.getName())
										.cpf(client.getCpf())
										.birthDate(client.getBirthDate())
										.build();
	}

}
