CREATE TABLE tb_client (
	id integer PRIMARY key DEFAULT nextval('client_id_seq'),
	name varchar(255) NOT NULL, 
	nr_cpf varchar(11) NOT NULL,
	dt_birth date NOT NULL
)